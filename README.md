Getting Started
---------------

### Installing WF+ and creating an instance

1.  Pull WFP_V2.edit, WFP_V2.editor, WFP_V2 from <https://github.com/t-chiang/WorkFlowPlus> into your eclipse workspace.

2.  Help-> Eclipse Marketplace -> Install the following: EcoreTools, Epsilon 

3.  Restart Eclipse

4.  Help-> Install new Software -> Works with -> <http://download.eclipse.org/sirius/updates/releases/6.6.0/2020-09> -> Install everything

5.  Restart Eclipse

6.  Right click on WFP_V2, Run As Eclipse Application

7.  Within the runtime, create a new modeling project.

8.  Right click the project, New, Other, search WFP_V2

9.  Select Work Flow Plus under Model Object, Finish

10. Select the Model Object and right click Work Flow Plus, create a new representation, select WFP and Finish

11. Import WFP.design from <https://github.com/t-chiang/WorkFlowPlus> in the runtime.

12. In WFP.design/description click on WFP.odesign. Right click WFP, New Layout, WF+ Layout

13. Close the runtime and restart the runtime

### WFP_V2.elk.layered.layout

-   LayoutProvider.java is the entry point when the "Auto-layout" buttton is pressed

-   Options are configured within the .melk file. More information can be found here on the .melk file

-   ELKLayoutExtension.java

-   ELK does not have access to Sirius Object elements

-   Workaround to give LayoutProvider access to the data type names (Data Element, Constraint etc)